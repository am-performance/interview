package internal

import (
	"net/http"
	"strconv"

	"gitlab.com/am-performance/interview/api/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	_ "github.com/mattn/go-sqlite3"
)

type Datastore struct {
	*gorm.DB
}

func New() *Datastore {
	return &Datastore{}
}

func (db *Datastore) Initialise() error {
	if db.DB == nil {
		store, err := gorm.Open(sqlite.Open("laps.db"), &gorm.Config{})
		if err != nil {
			return err
		}
		db.DB = store
		return store.AutoMigrate(&models.Lap{})

	}
	return nil
}

func (db *Datastore) Paginate(r *http.Request) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		q := r.URL.Query()
		page, _ := strconv.Atoi(q.Get("page"))
		if page == 0 {
			page = 1
		}

		pageSize, _ := strconv.Atoi(q.Get("page_size"))
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
