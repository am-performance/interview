package dal

import "gitlab.com/am-performance/interview/api/models"

// GetLapByPhase returns a specific phase for a given lap number
func (dal *DataAccessLayer) GetLapByPhase(num int, phase string) Response {
	data := []models.Lap{}
	dal.Datastore.
		Order("Distance asc").
		Where(&models.Lap{Lap: num, CornerPhase: phase}).
		Find(&data)

	return Response{
		Data:     data,
		Page:     dal.Page,
		PageSize: dal.PageSize,
		Total:    len(data),
	}
}
