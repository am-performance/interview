package dal

import "gitlab.com/am-performance/interview/api/models"

// GetPointInLap returns a data for a specific GPS point in a given lap
func (dal *DataAccessLayer) GetPointInLap(num int, lat, long float64) Response {
	data := []models.Lap{}
	dal.Datastore.
		Where(&models.Lap{Lap: num, Lateral: lat, Longitudinal: long}).
		Find(&data)

	return Response{
		Data:     data,
		Page:     dal.Page,
		PageSize: dal.PageSize,
		Total:    len(data),
	}
}
