package dal

import (
	"gitlab.com/am-performance/interview/api/models"
)

// SetLapData takes in a lap and posts a new data entry
func (dal *DataAccessLayer) SetLapData(lap *models.Lap) {
	dal.Create(lap)
}

// GetLap returns all data points for a given lap number
func (dal *DataAccessLayer) GetLap(num int) Response {
	data := []models.Lap{}
	dal.Datastore.
		Order("Distance asc").
		Where(&models.Lap{Lap: num}).
		Find(&data)

	return Response{
		Data:     data,
		Page:     dal.Page,
		PageSize: dal.PageSize,
		Total:    len(data),
	}
}

// GetLaps returns all lap data
func (dal *DataAccessLayer) GetLaps() Response {
	data := []models.Lap{}
	dal.Datastore.
		Order("Distance asc").
		Find(&data)

	return Response{
		Data:     data,
		Page:     dal.Page,
		PageSize: dal.PageSize,
		Total:    len(data),
	}
}
