package dal

import (
	"gitlab.com/am-performance/interview/api/models"
)

// GetLapByDrag returns all data for a given lap number orders byhighest drag
func (dal *DataAccessLayer) GetLapByDrag(num int) Response {
	data := []models.Lap{}

	dal.Datastore.
		Order("Drag desc").
		Where(&models.Lap{Lap: num}).
		Find(&data)

	return Response{
		Data:     data,
		Page:     dal.Page,
		PageSize: dal.PageSize,
		Total:    len(data),
	}
}
