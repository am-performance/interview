package dal

import (
	"net/http"
	"strconv"

	"gitlab.com/am-performance/interview/api/internal"
	"gitlab.com/am-performance/interview/api/models"
	"gitlab.com/bf86/lib/go/log"
)

type DataAccessLayer struct {
	*internal.Datastore
	Page, PageSize int
}

type Response struct {
	Data     []models.Lap `json:"data"`
	Page     int          `json:"page"`
	PageSize int          `json:"page_size,omitempty"`
	Total    int          `json:"total"`
}

type RequestParam string

const Offset RequestParam = "offset"
const Limit RequestParam = "limit"

func New() *DataAccessLayer {
	db := internal.New()
	if err := db.Initialise(); err != nil {
		log.Error("database initialisation failed", map[string]interface{}{"error": err})
	}

	return &DataAccessLayer{Datastore: db, Page: 1, PageSize: 10}
}

// WithPaging adds the ability to page requests (this defaults to a page size of 10)
func (dal *DataAccessLayer) WithPaging(r *http.Request) {
	dal.DB = dal.DB.Scopes(dal.Paginate(r))
	q := r.URL.Query()
	page, _ := strconv.Atoi(q.Get("page"))
	if page == 0 {
		dal.Page = 1
	}
	dal.Page = page

	pageSize, _ := strconv.Atoi(q.Get("page_size"))
	switch {
	case pageSize > 100:
		dal.PageSize = 100
	case pageSize <= 0:
		dal.PageSize = 10
	}
}
