package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/dal"
	"gitlab.com/am-performance/interview/api/handlers"
	"gitlab.com/bf86/lib/go/log"
)

var (
	API_PROTOCOL = "http"
	API_HOST     = "localhost"
	API_PORT     = "3000"
)

func configure() {
	if env, ok := os.LookupEnv("API_HOST"); ok {
		API_HOST = env
	}
	if env, ok := os.LookupEnv("API_PORT"); ok {
		API_PORT = env
	}
	if env, ok := os.LookupEnv("API_PROTOCOL"); ok {
		API_PROTOCOL = env
	}
}

func main() {
	configure()
	dataHandler := dal.New()

	router := mux.NewRouter().StrictSlash(true)
	router.Use(handlers.ApiMiddleware) // set API Headers
	router.Use(log.Middleware)         // Log all requests

	router = appendRoutes(router, dataHandler)

	log.Info(fmt.Sprintf("Listening on: %s://%s:%s", API_PROTOCOL, API_HOST, API_PORT), map[string]interface{}{
		"protocol": API_PROTOCOL,
		"host":     API_HOST,
		"port":     API_PORT,
	})

	if err := http.ListenAndServe(fmt.Sprintf("%s:%s", API_HOST, API_PORT), router); err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
}
