package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/dal"
)

func Drag(dal *dal.DataAccessLayer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		dal.WithPaging(r)
		var dataErrs []Error

		lapId := mux.Vars(r)["lap"]
		lap, err := strconv.Atoi(lapId)
		if err != nil {
			dataErrs = append(dataErrs, Error{
				Code:    http.StatusBadRequest,
				Message: "invalid lap",
				Error:   err,
			})
		}

		if len(dataErrs) > 0 {
			json.NewEncoder(w).Encode(dataErrs)
		} else {
			json.NewEncoder(w).Encode(dal.GetLapByDrag(lap))
		}
	}
}
