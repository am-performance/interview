package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/dal"
	"golang.org/x/exp/slices"
)

var phases = []string{
	"entry",
	"braking",
	"apex",
	"exit",
	"straight-line",
}

func Phase(dal *dal.DataAccessLayer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		dal.WithPaging(r)
		var dataErrs []Error

		if mux.Vars(r)["lap"] == "" {
			dataErrs = append(dataErrs, Error{
				Code:    http.StatusBadRequest,
				Message: "invalid lap number",
			})
		}

		lapId := mux.Vars(r)["lap"]
		lap, err := strconv.Atoi(lapId)
		if err != nil {
			dataErrs = append(dataErrs, Error{
				Code:    http.StatusBadRequest,
				Message: "invalid lap number",
				Error:   err,
			})
		}
		phase := strings.ToLower(mux.Vars(r)["phase"])

		if !slices.Contains(phases, phase) {
			dataErrs = append(dataErrs, Error{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("invalid phase. Phases available to query are: %s", strings.Join(phases, ", ")),
			})
		}

		if len(dataErrs) > 0 {
			json.NewEncoder(w).Encode(dataErrs)
			return
		} else {
			json.NewEncoder(w).Encode(dal.GetLapByPhase(lap, phase))
		}
	}
}
