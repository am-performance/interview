package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/dal"
	"gitlab.com/am-performance/interview/api/models"
)

func Laps(dal *dal.DataAccessLayer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		dal.WithPaging(r)
		var dataErrs []Error

		if r.Method == http.MethodGet {
			if mux.Vars(r)["lap"] == "" {
				json.NewEncoder(w).Encode(dal.GetLaps())
				return
			} else {
				lapId := mux.Vars(r)["lap"]
				lap, err := strconv.Atoi(lapId)
				if err != nil {
					dataErrs = append(dataErrs, Error{
						Code:    http.StatusBadRequest,
						Message: "invalid lap",
						Error:   err,
					})
				}

				if len(dataErrs) > 0 {
					json.NewEncoder(w).Encode(dataErrs)
					return
				} else {
					json.NewEncoder(w).Encode(dal.GetLap(lap))
					return
				}
			}
		}

		if r.Method == http.MethodPost {
			lap := &models.Lap{}
			json.NewDecoder(r.Body).Decode(&lap)
			dal.SetLapData(lap)
			w.WriteHeader(http.StatusCreated)
		}
	}
}
