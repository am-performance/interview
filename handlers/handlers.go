package handlers

type Error struct {
	Code    int
	Message string
	Error   error
}
