package handlers

import (
	"encoding/json"
	"net/http"
)

func Home(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(struct {
		Message string `json:"message"`
	}{
		Message: "Welcome to the Aston Martin Performance Telemetry API",
	})
}
