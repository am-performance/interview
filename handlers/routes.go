package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/models"
)

type Route struct {
	Path    string
	Methods []string
}

func DisplayRoutes(router *mux.Router) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var result []Route
		var dataErrs []*models.ErrorResponse

		router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
			var r Route
			if methods, err := route.GetMethods(); err != nil {
				dataErrs = append(dataErrs, &models.ErrorResponse{
					Code:    http.StatusBadRequest,
					Message: err.Error(),
				})
			} else {
				r.Methods = methods
			}
			if path, err := route.GetPathTemplate(); err != nil {
				dataErrs = append(dataErrs, &models.ErrorResponse{
					Code:    http.StatusBadRequest,
					Message: err.Error(),
				})
			} else {
				r.Path = path
			}
			if len(dataErrs) == 0 {
				result = append(result, r)
			}
			return nil
		})
		if len(dataErrs) > 0 {
			json.NewEncoder(w).Encode(dataErrs)
		} else {
			json.NewEncoder(w).Encode(result)
		}
	}
}
