## Aston Martin Track Data API

## Description
This project aims to expose track sensor data in a meaningful interface to enable engineers to request meaningful data combinations in order to make decisions on car performance.

This project is written entirely using Golang as the language of choice for both speed of development and speed of execution.

## Assumptions

I made a Proof of Concept which utilised [RabbitMQ](https://www.rabbitmq.com/) and a telemetry api to mock the data coming in from the car's sensors, as well as a listener to push the data to the API.

Given that the data was provided in csv format I decided not to persue this for too long because it would (and did) impact time spent on the actual solution.

If you'd like to see it I'd be happy to share the codebase for that also (it ran entirely inside `docker` & `docker compose`)

## Installation
This API is designed to be run using `docker compose`.

### Project Dependencies
 - [Docker Engine](https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script)
 - [Docker Compose](https://docs.docker.com/compose/install/)

**Optional**
 - [Go](https://go.dev/learn/): This will be useful if you want to make any changes.
 - [SQLite](https://sqlite.org/index.html): Useful for visualising the data outside of the API


## Environment Variables

| Variable     | Description                                                        |
| --------     | ------------------------------------------------------------------ |
| API_PROTOCOL | Used to aid switching between http/s for different environments    |
| API_HOST     | The Domain to host the API under, again useful for multi-env setup |
| API_PORT     | The Port to expose the API on, again useful for multi-env setup    |

## Testing

Apologies for the lack of tests here, generally these would be fully unit tested but to be frank I ran out of free time to give this the attention it deserved.

## Usage

There are several routes defined here, easiest way to see them all is via the `/_/routes` GET request to the API.

Displayed here for brevity:

```json
[
    {"Path": "/_/routes","Methods": ["GET"]}, // Show all API Routes
    {"Path": "/","Methods": ["GET"]}, // API Root 
    {"Path": "/laps","Methods": ["POST","OPTIONS"]}, // Add new Lap Data
    {"Path": "/laps","Methods": ["GET"]}, // Get All Lap Data 
    {"Path": "/laps/{lap:[0-9]+}","Methods": ["GET"]}, // Get Lap Data
    {"Path": "/laps/{lap:[0-9]+}/drag","Methods": ["GET"]}, // Get Lap Data orders by highest drag
    {"Path": "/laps/{lap:[0-9]+}/phase/{phase:[a-zA-Z-]+}","Methods": ["GET"]}, // Get Lap data by corner phase
    {"Path": "/laps/{lap:[0-9]+}/gps/{lat:[0-9.]+}/{long:[0-9.]+}","Methods": ["GET"]} // Get Lap Data for GPS position
]
```

## Closing Notes

In general, I wouldn't package the DB inside the docker image with the API unless it was a fixed-size cache. However this provided the smoothest development path during the build phase and would be useful as an API cache once the actual database had been abstracted outside the API.