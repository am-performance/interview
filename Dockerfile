##############################
# Stage 1: Prepare workspace #
##############################
FROM golang:alpine as base

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git gcc musl-dev sqlite
WORKDIR $GOPATH/api

COPY go.mod go.sum ./
# Fetch dependencies.
RUN go mod download


##################
# Stage 2: Build #
##################
FROM base AS compile
# Set go build envs
ENV GOOS="linux"
ENV CGO_ENABLED=1                                                                                                                                                                                                                                           
ENV GOARCH=amd64
# Copy app files
COPY . .
# Compile binary
RUN go build -o /go/bin/api -a -ldflags '-linkmode external -extldflags "-static"' .


###################
# Stage 3: Deploy #
###################
FROM scratch
COPY --from=compile /go/bin/api .
COPY internal/laps.db laps.db
COPY internal/data.csv data.csv
ENTRYPOINT ["/api"]
