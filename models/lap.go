package models

import (
	"gorm.io/gorm"
)

type Lap struct {
	gorm.Model               `json:"-"`
	Lap                      int     `csv:"LapNumber" json:"lap"`
	CornerPhase              string  `csv:"CornerPhase" json:"corner_phase"`
	Throttle                 float64 `csv:"ThrottlePedal" json:"throttle"`
	Brake                    float64 `csv:"BrakeForce" json:"brake"`
	LateralAcceleration      float64 `csv:"LateralAcceleration"  json:"acceleration_lateral"`
	LongitudinalAcceleration float64 `csv:"LongitudinalAcceleration" json:"acceleration_longitudinal"`
	FrontRideHeight          float64 `csv:"FRH" json:"front_ride_height"`
	FrontLiftCoefficient     float64 `csv:"CzF" json:"front_lift_coefficient"`
	FrontSecondaryFlapAngle  float64 `csv:"FlapAngle" json:"front_secondary_flap_angle"`
	RearRideHeight           float64 `csv:"RRH" json:"rear_ride_height"`
	RearLiftCoefficient      float64 `csv:"CzR" json:"rear_lift_coefficient"`
	DragCoefficient          float64 `csv:"Cx" json:"drag_coefficient"`
	Lateral                  float64 `csv:"GPSLatCoord" json:"lateral_gps_coordinates"`
	Longitudinal             float64 `csv:"GPSLongCoord" json:"longitudinal_gps_coordinates"`
	Distance                 float64 `csv:"LapDistance" json:"distance"`
	Speed                    float64 `csv:"Speed" json:"speed"`
	Yaw                      float64 `csv:"Yaw" json:"yaw"`
	Steer                    float64 `csv:"Steer" json:"steer"`
	Roll                     float64 `csv:"Roll" json:"roll"`
}
