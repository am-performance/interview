package models

type ErrorResponse struct {
	Code    int    `json:"-"`
	Message string `json:"message"`
}
