package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/am-performance/interview/api/dal"
	"gitlab.com/am-performance/interview/api/handlers"
)

func appendRoutes(router *mux.Router, dal *dal.DataAccessLayer) *mux.Router {

	router.Methods(http.MethodGet).Path("/_/routes").
		HandlerFunc(handlers.DisplayRoutes(router))

	router.Methods(http.MethodGet).Path("/").
		HandlerFunc(handlers.Home)

	// Post new lap data
	router.Methods(http.MethodPost, http.MethodOptions).Path("/laps").
		HandlerFunc(handlers.Laps(dal))

	// Fetch All Lap data
	router.Methods(http.MethodGet).Path("/laps").
		HandlerFunc(handlers.Laps(dal))

	// Fetch Lap data by Lap Number
	router.Methods(http.MethodGet).Path("/laps/{lap:[0-9]+}").
		HandlerFunc(handlers.Laps(dal))

	// Fetch Lap data sorted by highest Drag
	router.Methods(http.MethodGet).Path("/laps/{lap:[0-9]+}/drag").
		HandlerFunc(handlers.Drag(dal))

	// Fetch Lap data by corner phase
	router.Methods(http.MethodGet).Path("/laps/{lap:[0-9]+}/phase/{phase:[a-zA-Z-]+}").
		HandlerFunc(handlers.Phase(dal))

	// Fetch Lap data by specific GPS Lat & Long
	router.Methods(http.MethodGet).Path("/laps/{lap:[0-9]+}/gps/{lat:[0-9.]+}/{long:[0-9.]+}").
		HandlerFunc(handlers.GPS(dal))

	return router
}
