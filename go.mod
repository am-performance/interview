module gitlab.com/am-performance/interview/api

go 1.19

require (
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.15
	gitlab.com/bf86/lib/go/log v0.0.2
	golang.org/x/exp v0.0.0-20220827204233-334a2380cb91
	gorm.io/driver/sqlite v1.3.6
	gorm.io/gorm v1.23.8
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
